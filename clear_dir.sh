#!/usr/bin/env bash

find ./ -type d                        \
        -regextype posix-egrep         \
        -regex ".*(?:/[a-z]{9})?"      \
        -exec rm -rf {} \; 2>/dev/null