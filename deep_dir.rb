#!/usr/bin/env ruby

def rand_name
  (0..8).map { ('a'..'z').to_a[rand(26)] }.join
end

def deep_dir(deepness)
  deepness.times.map { rand_name }.join('/')
end

10000.times { `mkdir -p #{deep_dir(33)}` }